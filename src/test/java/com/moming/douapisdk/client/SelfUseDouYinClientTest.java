package com.moming.douapisdk.client;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.properties.DouYinProperties;
import com.moming.douapisdk.request.ProductListRequest;
import com.moming.douapisdk.response.ProductListResponse;
import com.moming.douapisdk.storage.DefaultConfigStorageImpl;
import com.moming.douapisdk.storage.IDouYinConfigStorage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 自用型应用测试接口
 *
 *
 * @author lujingpo
 * @date 2021/2/5
 */
@SpringBootTest
@ActiveProfiles(value = "self-use")
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
class SelfUseDouYinClientTest {

    @Autowired
    private DouYinProperties douYinProperties;

    @Test
    void name() throws ApiException {
        IDouYinConfigStorage configStorage = new DefaultConfigStorageImpl(douYinProperties);
        SelfUseDouYinClient douYinClient = new SelfUseDouYinClient(configStorage);

        ProductListRequest request = new ProductListRequest();
        request.setPage("0");
        request.setSize("100");

        ProductListResponse response = douYinClient.execute(request);

        assertNotNull(response.getData());
    }
}