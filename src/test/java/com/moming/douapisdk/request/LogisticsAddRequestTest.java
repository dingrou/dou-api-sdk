package com.moming.douapisdk.request;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.client.SelfUseDouYinClient;
import com.moming.douapisdk.response.LogisticsAddResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Map;

/**
 * 测试物流发货
 *
 * @author lujingpo
 * @date 2021/3/15
 */
@SpringBootTest
@ActiveProfiles(value = "self-use")
@Slf4j
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
class LogisticsAddRequestTest {


    @Autowired
    private DouYinClient defaultDouYinClient;



    /**
     * 批量发货api接口
     */
    @Test
    void batchLogisticsAdd() {
        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        // 循环文件
        String fileName = "/Users/lujingpo/Downloads/手动修改订单（抖音）.xlsx";
        if (FileUtil.isFile(fileName)) {
            ExcelReader reader = ExcelUtil.getReader(fileName);
            List<Map<String,Object>> rows = reader.readAll();

            LogisticsAddRequest request = new LogisticsAddRequest();

            for (Map<String, Object> row : rows) {
                request.setLogisticsId("7");
                request.setOrderId(MapUtil.getStr(row, "订单号").replace("\t", "")+"A");
                request.setLogisticsCode(MapUtil.getStr(row, "快递单号").replace("\t", ""));
                try {
                    LogisticsAddResponse execute = defaultDouYinClient.execute(request);
                } catch (ApiException e) {
                    log.error(e.getMessage());
                }
            }
        }




        // 调用接口

        // 收集结果
        stopWatch.stop();
        stopWatch.prettyPrint();
    }
}