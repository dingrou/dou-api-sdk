package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DefaultDouYinClient;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.client.SelfUseDouYinClient;
import com.moming.douapisdk.response.RefundProcessDetailResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/3/1
 */
@SpringBootTest
@ActiveProfiles(value = "local")
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
class RefundProcessDetailRequestTest {

    @Autowired
    private DouYinClient selfUseDouYinClient;

    @Test
    void test() throws ApiException {
        RefundProcessDetailRequest request = new RefundProcessDetailRequest();
        request.setOrderId("4810345586066656895");

        RefundProcessDetailResponse execute = selfUseDouYinClient.execute(request);


    }
}