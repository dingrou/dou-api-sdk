package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.domain.CipherInfo;
import com.moming.douapisdk.response.OrderBatchDecryptResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 批量解密测试
 *
 * @author LangYu
 * @date 2021/7/10
 */
@SpringBootTest
@ActiveProfiles(value = "local")
// @ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
public class OrderBatchDecryptTest {

    @Autowired
    private DouYinClient douYinClient;

    private final String accessToken = "b7a02bea-b85b-4c45-8aca-1106f9c49f7e";

    @Test
    void test() throws ApiException {
        CipherInfo c = new CipherInfo();
        c.setAuthId("4825487712306867992");
        c.setCipherText("$z+Kl6BIYGliCVDipHgEx6J9uFZOykrVfulpfc8N0WD0=$zM7zovdX1wKienpru8HYCc1FQaLwl+Toeiamzl8Pfv7tAwwrcb2a7/9rRP1S7TQuqTS9a4gbEgmzS5WPu8nuzXq6BtbqrSZOFETfF5k=*CgkIARCtHCABKAESPgo8uh/H8slCf69xOyAjZfu5MqlMetErR2S6Q8THTARQrPQ0YiNPXTZ6mR1voa51Ltw7IX1KDcInzAG7aA41GgA=$1$$");

        List<CipherInfo> list = new ArrayList<CipherInfo>();
        list.add(c);
        OrderBatchDecryptRequest request = new OrderBatchDecryptRequest();
        request.setCipherInfoList(list);
        OrderBatchDecryptResponse execute = douYinClient.execute(request, accessToken);
        assertNotNull(execute);
    }
}
