package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DefaultDouYinClient;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.OrderSearchListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/6/6
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class OrderSearchListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    private final String accessToken = "b7a02bea-b85b-4c45-8aca-1106f9c49f7e";


    @Test
    void testList() throws ApiException {
        OrderSearchListRequest request = new OrderSearchListRequest();
        long epochSecond = LocalDateTime.now().with(LocalTime.MIN)
                .toEpochSecond(ZoneOffset.ofHours(8));
        request.setCreateTimeStart(epochSecond);
        request.setPage(0);
        request.setSize(1);

        OrderSearchListResponse execute = douYinClient.execute(request, accessToken);

        assertNotNull(execute);
    }
}