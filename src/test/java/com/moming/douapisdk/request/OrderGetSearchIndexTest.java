package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.client.SelfUseDouYinClient;
import com.moming.douapisdk.properties.DouYinProperties;
import com.moming.douapisdk.request.OrderGetSearchIndexRequest;
import com.moming.douapisdk.request.OrderSearchListRequest;
import com.moming.douapisdk.response.OrderGetSearchIndexResponse;
import com.moming.douapisdk.response.OrderSearchListResponse;
import com.moming.douapisdk.storage.DefaultConfigStorageImpl;
import com.moming.douapisdk.storage.IDouYinConfigStorage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author LangYu
 * @date 2021/7/10
 */
@SpringBootTest
@ActiveProfiles(value = "local")
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
public class OrderGetSearchIndexTest {
    @Autowired
    private DouYinClient douYinClient;


    private final String accessToken = "edae7c30-8386-443b-88a1-031111596fdd";

    @Test
    void test() throws ApiException {

        OrderGetSearchIndexRequest request = new OrderGetSearchIndexRequest();
        request.setPlainText("余其虹");
        request.setSensitiveType(2);
        OrderGetSearchIndexResponse execute = douYinClient.execute(request, accessToken);
        assertNotNull(execute);
    }
}
