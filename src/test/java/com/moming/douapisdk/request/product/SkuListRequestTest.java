package com.moming.douapisdk.request.product;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.product.SkuListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2022/3/22
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class SkuListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    @Test
    void testInvoke() throws ApiException {
        SkuListRequest skuListRequest = new SkuListRequest();
        skuListRequest.setProductId(3511458752689786486L);

        SkuListResponse execute = douYinClient.execute(skuListRequest);
        assertNotNull(execute);
    }
}