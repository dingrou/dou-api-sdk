package com.moming.douapisdk.request.product;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.request.CommentListRequest;
import com.moming.douapisdk.response.CommentListResponse;
import com.moming.douapisdk.response.ProductListResponse;
import com.moming.douapisdk.response.product.ProductListV2Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2022/1/25
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class ProductListV2RequestTest {

    @Autowired
    private DouYinClient douYinClient;


    @Test
    void testRequest() throws ApiException {
        ProductListV2Request request = new ProductListV2Request();
        request.setPage(1L);
        request.setSize(100L);

        ProductListV2Response execute = douYinClient.execute(request);

        assertTrue(true);
    }
}