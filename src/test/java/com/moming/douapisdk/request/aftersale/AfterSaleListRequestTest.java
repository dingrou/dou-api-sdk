package com.moming.douapisdk.request.aftersale;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.aftersale.AfterSaleListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2022/1/25
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class AfterSaleListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    @Test
    void testRequest() throws ApiException {
        AfterSaleListRequest request = new AfterSaleListRequest();
        request.setPage(0L);
        request.setSize(100L);
        request.setUpdateStartTime(LocalDateTime.now());
        AfterSaleListResponse execute = douYinClient.execute(request);

        System.out.println(true);

    }
}