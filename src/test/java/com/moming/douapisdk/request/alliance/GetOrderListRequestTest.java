package com.moming.douapisdk.request.alliance;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.alliance.GetOrderListResponse;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/12/13
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class GetOrderListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    private final String accessToken = "b7a02bea-b85b-4c45-8aca-1106f9c49f7e";

    @Test
    void testExecute() throws ApiException {
        GetOrderListRequest getOrderListRequest = new GetOrderListRequest();
        getOrderListRequest.setOrderIds(Lists.newArrayList("4889121367946007720"));
        GetOrderListResponse execute = douYinClient.execute(getOrderListRequest);
        System.out.println(execute.getDatas());
    }
}