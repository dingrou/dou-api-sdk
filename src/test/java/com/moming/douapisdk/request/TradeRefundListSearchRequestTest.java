package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.TradeRefundListSearchResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author lujingpo
 * @date 2021/2/5
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class TradeRefundListSearchRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    private String accessToken = "b7a02bea-b85b-4c45-8aca-1106f9c49f7e";

    @Test
    void testApi() throws ApiException {
        TradeRefundListSearchRequest request = new TradeRefundListSearchRequest();
        request.setPage(0);
        request.setSize(1);
        request.setStartTime(LocalDateTime.now().minusMonths(1));
        request.setIsDesc(0);
        request.setOrderBy(TradeRefundListSearchRequest.ORDER_BY.UPDATE_TIME);

        TradeRefundListSearchResponse execute = douYinClient.execute(request, accessToken);

        assertTrue(execute.getAfterSaleList().size() > 0);
    }

}