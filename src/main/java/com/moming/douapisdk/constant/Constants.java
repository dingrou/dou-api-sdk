package com.moming.douapisdk.constant;

import java.util.Arrays;
import java.util.List;

/**
 * @author tianzong
 * @date 2020/7/21
 */
public interface Constants {
    /** 协议入参共享参数 **/
    String APP_KEY = "app_key";
    String METHOD = "method";
    String TIMESTAMP = "timestamp";
    String VERSION = "v";
    String SIGN = "sign";
    String ACCESS_TOKEN = "access_token";
    String PARAM_JSON = "param_json";

    /**
     * 授权类型：工具型授权
     */
    String AUTHORIZATION_CODE = "authorization_code";

    /**
     * 授权类型：自用型授权
     */
    String AUTHORIZATION_SELF = "authorization_self";

    String REFRESH_GRANT_TYPE = "refresh_token";
    /**
     * 授权访问地址
     */
    String AUTHORIZATION_PATH = "/token/create";

    /**
     * 刷新token的地址
     */
    String REFRESH_TOKEN_PATH = "/token/refresh";


    /** 协议出参共享参数 */
    String ERROR_CODE = "err_no";
    String ERROR_MSG = "message";

    /** 默认时间格式 **/
    String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** Date默认时区 **/
    String DATE_TIMEZONE = "GMT+8";

    /** UTF-8字符集 **/
    String CHARSET_UTF8 = "UTF-8";

    /** HTTP请求相关 **/
    String METHOD_POST = "POST";
    String METHOD_GET = "GET";
    String C_TYPE_FORM_DATA = "application/x-www-form-urlencoded";
    String C_TYPE_FILE_UPLOAD = "multipart/form-data";
    String C_TYPE_TEXT_XML = "text/xml";
    String C_TYPE_APPLICATION_XML = "application/xml";
    String C_TYPE_TEXT_PLAIN = "text/plain";
    String C_TYPE_APP_JSON = "application/json";

    /**
     * token异常，需要刷新token
     */
    List<Integer> ACCESS_TOKEN_ERROR_CODES = Arrays.asList(
            DouYinErrorCodeEnum.CODE_30001.getCode(),
            DouYinErrorCodeEnum.CODE_30002.getCode(),
            DouYinErrorCodeEnum.CODE_30005.getCode());

    /**
     * 抖音api访问频繁
     */
    List<Integer> VISIT_TOO_MANY_ERROR_CODES = Arrays.asList(
            DouYinErrorCodeEnum.CODE_8.getCode(),
            DouYinErrorCodeEnum.CODE_9.getCode());
}
