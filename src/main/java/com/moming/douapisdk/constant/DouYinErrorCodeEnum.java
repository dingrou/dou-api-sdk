package com.moming.douapisdk.constant;

import lombok.Getter;

/**
 * <pre>
 *     抖店错误码
 *     参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/10/23">返回错误码列表</a>
 * </pre>
 *
 * @author lujingpo
 * @date 2021/2/3
 */
@Getter
public enum DouYinErrorCodeEnum {

    /**
     * 请求成功
     */
    CODE_0(0, "请求成功"),
    /**
     * 请登录后再操作
     */
    CODE_1(1, "请登录后再操作"),
    /**
     * 无权限
     */
    CODE_2(2, "无权限"),
    /**
     * 缺少参数
     */
    CODE_3(3, "缺少参数"),
    /**
     * 参数错误
     */
    CODE_4(4, "参数错误"),
    /**
     * 参数不合法
     */
    CODE_5(5, "参数不合法"),
    /**
     * 业务参数json解析失败, 所有参数需为string类型
     */
    CODE_6(6, "业务参数json解析失败, 所有参数需为string类型"),
    /**
     * 服务器错误
     */
    CODE_7(7, "服务器错误"),
    /**
     * 服务繁忙
     */
    CODE_8(8, "服务繁忙"),
    /**
     * 访问太频繁
     */
    CODE_9(9, "访问太频繁"),
    /**
     * 需要用 POST 请求
     */
    CODE_10(10, "需要用 POST 请求"),
    /**
     * 签名校验失败
     */
    CODE_11(11, "签名校验失败"),
    /**
     * 版本太旧,请升级
     */
    CODE_12(12, "版本太旧,请升级"),
    /**
     * 找不到user_id
     */
    CODE_302(302, "找不到user_id"),
    /**
     * 认证失败,app_key不存在,格式不正确,应为19位纯数字,access_token不能为空
     */
    CODE_30001(30001, "认证失败,app_key不存在,格式不正确,应为19位纯数字,access_token不能为空"),
    /**
     * access_token已过期
     */
    CODE_30002(30002, "access_token已过期"),
    /**
     * 店铺授权已失效,请重新引导商家完成店铺授权
     */
    CODE_30003(30003, "店铺授权已失效,请重新引导商家完成店铺授权"),
    /**
     * 应用已被系统禁用
     */
    CODE_30004(30004, "应用已被系统禁用"),
    /**
     * access_token不存在,请使用最新的access_token访问
     */
    CODE_30005(30005, "access_token不存在,请使用最新的access_token访问"),
    /**
     * 店铺授权已被关闭,请联系商家打开授权开关
     */
    CODE_30006(30006, "店铺授权已被关闭,请联系商家打开授权开关"),
    /**
     * app_key和access_token不匹配,请仔细检
     */
    CODE_30007(30007, "app_key和access_token不匹配,请仔细检查"),
    /**
     * 生成token失败, grant_type参数取值不正确,请参照文档根据应用类型填写不同的值
     */
    CODE_31003(31003, "生成token失败, grant_type参数取值不正确,请参照文档根据应用类型填写不同的值");



    DouYinErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 错误码
     */
    private final int code;

    /**
     * 错误信息
     */
    private final String message;
}
