package com.moming.douapisdk.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;


/**
 * @author tianzong
 * @date 2020/7/23
 */
@Data
@ConfigurationProperties(prefix = "spring.dou-yin")
@Validated
public class DouYinProperties {

    /**
     * api调用地址
     */
    private String apiUrl = "https://openapi-fxg.jinritemai.com";

    /**
     * appKey
     */
    @NotNull
    private String appKey;

    /**
     * appSecret
     */
    @NotNull
    private String appSecret;

    //region 自研型应用专用配置
    /**
     * 店铺id
     * <p>
     *     仅自研型应用使用，
     *     <ul>
     *         <li>设置，取自研型应用中对应shopId的店铺</li>
     *         <li>不设置，自动取第一个授权的店铺</li>
     *     </ul>
     *
     * </p>
     *
     */
    private String shopId;

    /**
     * 是否是自研型应用
     */
    private boolean isSelfUse = false;

    /**
     * token过期的时候，是否自动刷新
     * <p>仅针对自研型应用</p>
     */
    private boolean autoRefresh = false;
    //endregion

}


