package com.moming.douapisdk.storage;

import java.util.concurrent.locks.Lock;

/**
 * 配置存储实现
 *
 * 自用型的应用需要存储和维护token
 *
 * @author lujingpo
 * @date 2021/2/3
 */
public interface IDouYinConfigStorage {

    /**
     * 获取accessToken
     * @return String
     */
    String getAccessToken();

    /**
     * 更新accessToken
     * @param accessToken token
     * @param refreshToken 刷新令牌
     * @param expiresInSeconds 过期时间
     */
    void updateAccessToken(String accessToken, String refreshToken, int expiresInSeconds);

    /**
     * 获取刷新令牌
     * @return 刷新令牌
     */
    String getRefreshToken();

    /**
     * 是否已经过期
     * @return 是否过期
     */
    boolean isRefreshTokenExpired();

    /**
     * 获取更新token的锁
     * @return 锁
     */
    Lock getAccessTokenLock();

    /**
     * 是否已经过期
     * @return 是否过期
     */
    boolean isAccessTokenExpired();

    /**
     * 获取appKey
     * @return 结果
     */
    String getAppKey();

    /**
     * 设置appKey
     * @param appKey key
     */
    void setAppKey(String appKey);

    /**
     * 获取店铺id
     * @return 获取店铺id
     */
    String getShopId();

    /**
     * 设置店铺id
     * @param shopId 店铺id
     */
    void setShopId(String shopId);

    /**
     * 获取appSecret
     * @return 结果
     */
    String getAppSecret();

    /**
     * 设置apiUrl
     * @param apiUrl key
     */
    void setApiUrl(String apiUrl);

    /**
     * 获取apiUrl
     * @return 结果
     */
    String getApiUrl();


    /**
     * 设置secret
     * @param secret 密钥
     */
    void setAppSecret(String secret);

    /**
     * 强制将access token过期掉.
     */
    void expireAccessToken();

    /**
     * 是否自动刷新token
     *
     * @return .
     */
    boolean autoRefreshToken();

}
