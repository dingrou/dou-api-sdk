package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.request.OrderSearchListRequest;
import lombok.Data;

/**
 * 状态组合查询
 *
 * 订单检索接口使用{@link OrderSearchListRequest#getCombineStatus()}
 *
 * <a href="https://op.jinritemai.com/docs/api-docs/15/544"> 订单检索</a>
 *
 * @author lujingpo
 * @date 2021/6/6
 */
@Data
public class CombineStatus {

    /**
     * 订单状态，","分隔多个状态
     */
    @JSONField(name = "order_status")
    private String orderStatus;

    /**
     * 主流程状态，","分隔多个状态
     */
    @JSONField(name = "main_status")
    private String mainStatus;

}
