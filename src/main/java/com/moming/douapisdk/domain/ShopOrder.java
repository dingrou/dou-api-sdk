package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 店铺订单
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class ShopOrder {

    /**
     * 店铺ID
     */
    @JSONField(name = "app_id")
    private Integer appId;

    /**
     * app渠道
     * <p></p>
     * Example:
     * <ul>
     * <li>0 站外</li>
     * <li>1 火山</li>
     * <li>2 抖音</li>
     * <li>3 头条</li>
     * <li>4 西瓜</li>
     * <li>5 微信</li>
     * <li>6 值点app</li>
     * <li>7 头条lite</li>
     * <li>8 懂车帝</li>
     * <li>9 皮皮虾</li>
     * <li>11 抖音极速版</li>
     * <li>12 TikTok</li>
     * <li>13 musically</li>
     * <li>14 穿山甲</li>
     * <li>15 火山极速版</li>
     * <li>16 服务市场</li>
     * </ul>
     */
    @JSONField(name = "b_type")
    private Integer bType;
    /**
     * 下单端描述
     */
    @JSONField(name = "b_type_desc")
    private String bTypeDesc;

    /**
     * 业务来源
     * Example:
     * <blockquote>
     * <ul>
     * <li>1-鲁班</li>
     * <li>2-小店</li>
     * <li>3-好好学习等</li>
     * </ul>
     * </blockquote>
     */
    private Integer biz;
    /**
     * 业务来源描述
     */
    @JSONField(name = "biz_desc")
    private String bizDesc;
    /**
     * 买家留言
     */
    @JSONField(name = "buyer_words")
    private String buyerWords;
    /**
     * 取消原因
     */
    @JSONField(name = "cancel_reason")
    private String cancelReason;
    /**
     * 支付流水号
     */
    @JSONField(name = "channel_payment_no")
    private String channelPaymentNo;
    /**
     * 下单时间
     * <p/>
     * 秒级别时间戳<br/>
     * Example: 1622977290
     */
    @JSONField(name = "create_time")
    private Long createTime;
    /**
     * 密文出参
     * <p/>
     * 参考文档: <a href="https://bytedance.feishu.cn/docs/doccnJNKML3jOzFgjJitzXy61lh#">加解密指南</a>
     */
    @JSONField(name = "encrypt_post_receiver")
    private String encryptPostReceiver;
    /**
     * 收件人电话
     *
     */
    @JSONField(name = "encrypt_post_tel")
    private String encryptPostTel;
    /**
     * 预计发货时间
     * <p/>
     * 秒级时间戳 <br/>
     * Example: 1622977290
     */
    @JSONField(name = "exp_ship_time")
    private Long expShipTime;
    /**
     * 订单完成时间
     * <p/>
     * 秒级时间戳 <br/>
     * Example: 1622977290
     */
    @JSONField(name = "finish_time")
    private Long finishTime;
    /**
     * 物流信息
     */
    @JSONField(name = "logistics_info")
    private List<LogisticsInfo> logisticsInfo;
    /**
     * 主流程状态
     * <p/>
     * Example: 103
     * <br/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "main_status")
    private Integer mainStatus;
    /**
     * 主流程状态描述
     * <p/>
     * Example: 部分支付
     */
    @JSONField(name = "main_status_desc")
    private String mainStatusDesc;
    /**
     * 改价金额变化量
     * <p/>
     * Example: -10
     */
    @JSONField(name = "modify_amount")
    private Integer modifyAmount;
    /**
     * 改价运费金额变化量
     * <p/>
     * Example: -1
     */
    @JSONField(name = "modify_post_amount")
    private Integer modifyPostAmount;
    /**
     * 抖音小程序id
     */
    @JSONField(name = "open_id")
    private String openId;
    /**
     * 订单金额（分）
     */
    @JSONField(name = "order_amount")
    private Integer orderAmount;
    /**
     * 订单过期时间
     * <p/>
     * 单位秒 <br/>
     * Example: 1800
     *
     */
    @JSONField(name = "order_expire_time")
    private Long orderExpireTime;
    /**
     * 店铺订单号
     */
    @JSONField(name = "order_id")
    private String orderId;
    /**
     * 订单层级
     *
     */
    @JSONField(name = "order_level")
    private Integer orderLevel;
    /**
     * 定金预售阶段单
     */
    @JSONField(name = "order_phase_list")
    private List<OrderPhase> orderPhaseList;
    /**
     * 订单状态
     * <p/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "order_status")
    private Integer orderStatus;
    /**
     * 订单状态描述
     */
    @JSONField(name = "order_status_desc")
    private String orderStatusDesc;
    /**
     * 订类型
     * <p/>
     * <blockquote>
     * <ul>
     * <li>0-普通订单</li>
     * <li>2-虚拟订单</li>
     * <li>4-平台券码</li>
     * <li>5-商家券码</li>
     * </ul>
     * </blockquote>
     *
     */
    @JSONField(name = "order_type")
    private Integer orderType;
    /**
     * 订单类型描述
     */
    @JSONField(name = "order_type_desc")
    private String orderTypeDesc;
    /**
     * 支付金额（分）
     */
    @JSONField(name = "pay_amount")
    private Integer payAmount;
    /**
     * 支付时间
     * <p/>
     * 秒级时间戳 <br/>
     */
    @JSONField(name = "pay_time")
    private Long payTime;
    /**
     * 支付类型
     * <p/>
     * <ul>
     * <li>0-货到付款</li>
     * <li>1-微信</li>
     * <li>2-支付宝</li>
     * </ul>
     */
    @JSONField(name = "pay_type")
    private Integer payType;
    /**
     * 平台优惠金额平台承担部分
     * <p/>
     * 单位：分
     */
    @JSONField(name = "platform_cost_amount")
    private Integer platformCostAmount;
    /**
     * 收件人地址
     */
    @JSONField(name = "post_addr")
    private PostAddr postAddr;
    /**
     * 快递费（分）
     */
    @JSONField(name = "post_amount")
    private Integer postAmount;
    /**
     * 运费险金额
     */
    @JSONField(name = "post_insurance_amount")
    private Integer postInsuranceAmount;
    /**
     * 收件人姓名
     * @deprecated 数据加密完成之后，需要使用 {@link #encryptPostReceiver}
     */
    @JSONField(name = "post_receiver")
    private String postReceiver;
    /**
     * 收件人手机号
     * @deprecated 数据加密完成之后，需要使用 {@link #encryptPostTel}
     */
    @JSONField(name = "post_tel")
    private String postTel;
    /**
     * 单优惠总金额
     * <p/>
     * 计算方法：
     * <blockquote>单优惠总金额 = 店铺优惠金额+ 平台优惠金额+ 达人优惠金额+ 支付优惠金额</blockquote>
     */
    @JSONField(name = "promotion_amount")
    private Integer promotionAmount;
    /**
     * 支付优惠金额
     */
    @JSONField(name = "promotion_pay_amount")
    private Integer promotionPayAmount;
    /**
     * 平台优惠金额
     */
    @JSONField(name = "promotion_platform_amount")
    private Integer promotionPlatformAmount;
    /**
     * 店铺优惠金额
     */
    @JSONField(name = "promotion_shop_amount")
    private Integer promotionShopAmount;
    /**
     * 达人优惠金额
     */
    @JSONField(name = "promotion_talent_amount")
    private Integer promotionTalentAmount;
    /**
     * 卖家订单标记
     * <p/>
     * 小旗子star取值0～5，分别表示
     * <ul>
     * <li>0-灰</li>
     * <li>1-紫</li>
     * <li>2-青</li>
     * <li>3-绿</li>
     * <li>4-橙</li>
     * <li>5-红</li>
     * </ul>
     */
    @JSONField(name = "seller_remark_stars")
    private Integer sellerRemarkStars;
    /**
     * 商家备注
     */
    @JSONField(name = "seller_words")
    private String sellerWords;
    /**
     * 发货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "ship_time")
    private Long shipTime;
    /**
     * 平台优惠金额卖家承担部分
     * <p/>
     * 单位：分
     */
    @JSONField(name = "shop_cost_amount")
    private Integer shopCostAmount;
    /**
     * 店铺ID
     */
    @JSONField(name = "shop_id")
    private Long shopId;
    /**
     * 商户名称
     */
    @JSONField(name = "shop_name")
    private String shopName;
    /**
     * 商品单信息
     */
    @JSONField(name = "sku_order_list")
    private List<SkuOrder> skuOrderList;
    /**
     * 下单场景
     * <p/>
     * 可选值：
     * <ul>
     * <li>0 未知</li>
     * <li>1 app</li>
     * <li>2 小程序</li>
     * <li>3 H5</li>
     * </ul>
     */
    @JSONField(name = "sub_b_type")
    private Integer subBType;
    /**
     * 下单场景描述
     */
    @JSONField(name = "sub_b_type_desc")
    private String subBTypeDesc;

    /**
     * 交易类型
     * <p/>
     * 可选值：
     * <ul>
     * <li>1 拼团订单</li>
     * <li>2 定金预售</li>
     * </ul>
     */
    @JSONField(name = "trade_type")
    private Integer tradeType;
    /**
     * 交易类型描述
     */
    @JSONField(name = "trade_type_desc")
    private String tradeTypeDesc;
    /**
     * 订单更新时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "update_time")
    private Long updateTime;

    /**
     * 优惠信息
     */
    @JSONField(name = "promotion_detail")
    private PromotionDetail promotionDetail;

    /**
     * 加密用户ID串
     */
    @JSONField(name = "doudian_open_id")
    private String doudianOpenId;


}
