package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 带脱敏密文
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class CipherInfo {

    /**
     * 鉴权ID
     */
    @JSONField(name = "auth_id")
    private String authId;


    /**
     * 密文
     */
    @JSONField(name = "cipher_text")
    private String cipherText;

}
