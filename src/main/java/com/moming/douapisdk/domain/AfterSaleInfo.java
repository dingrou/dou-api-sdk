package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 售后信息
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class AfterSaleInfo {


    /**
     * 售后状态
     * <p/>
     * 可选值：
     * <ul>
     * <li>0-售后初始化</li>
     * <li>6-售后申请</li>
     * <li>7-售后退货中</li>
     * <li>27-拒绝售后申请</li>
     * <li>12-售后成功</li>
     * <li>28-售后失败</li>
     * <li>11-售后已发货</li>
     * <li>29-退货后拒绝退款</li>
     * <li>13-售后换货商家发货</li>
     * <li>14-售后换货用户收货</li>
     * <li>51-取消成功</li>
     * <li>53-逆向交易完成</li>
     * </ul>
     * */
    @JSONField(name = "after_sale_status")
    private Integer afterSaleStatus;

    /**
     * 售后类型
     * <p/>
     * <ul>
     * <li>0-售后退货退款</li>
     * <li>1-售后退款</li>
     * <li>2-售前退款</li>
     * <li>3-换货</li>
     * <li>4-系统取消</li>
     * <li>5-用户取消</li>
     * </ul>
     */
    @JSONField(name = "after_sale_type")
    private Integer afterSaleType;

    /**
     * 退款状态
     * <p/>
     * <ul>
     * <li>0-无需退款</li>
     * <li>1-待退款</li>
     * <li>2-退款中</li>
     * <li>3-退款成功</li>
     * <li>4-退款失败</li>
     * </ul>
     */
    @JSONField(name = "refund_status")
    private Integer refundStatus;

}
