package com.moming.douapisdk.domain;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

/**
 * 抖音商品
 *
 * @author lujingpo
 */
@Data
public class DouYinProduct {

    /**
     * 商品id
     */
    @JSONField(name = "product_id")
    private Long productId;
    /**
     *
     */
    @JSONField(name = "open_user_id")
    private Long openUserId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 详情html
     */
    private String description;

    /**
     * 头图，主图第一张
     */
    private String img;
    /**
     * 划线价，单位分
     */
    @JSONField(name = "market_price")
    private Integer marketPrice;

    /**
     * 售价，单位分
     */
    @JSONField(name = "discount_price")
    private Integer discountPrice;

    /**
     * 商品上下架状态：0上架 1下架 2已删除
     */
    private Integer status;

    /**
     * 规格id
     */
    @JSONField(name = "spec_id")
    private Long specId;

    /**
     * 商品审核状态：1未提审 2审核中 3审核通过 4审核驳回 5封禁
     */
    @JSONField(name = "check_status")
    private Integer checkStatus;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 一级类目
     */
    @JSONField(name = "first_cid")
    private Integer firstCid;

    /**
     * 二级类目
     */
    @JSONField(name = "second_cid")
    private Integer secondCid;

    /**
     * 三级类目
     */
    @JSONField(name = "third_cid")
    private Integer thirdCid;

    /**
     * 支持的支付方式：0货到付款 1在线支付 2两者都支持
     */
    @JSONField(name = "pay_type")
    private Integer payType;

    /**
     * 商家推荐语
     */
    @JSONField(name = "recommend_remark")
    private String recommendRemark;

    /**
     * 创建时间
     */
    @JSONField(name = "create_time")
    private String createTime;

    /**
     * 更新时间
     */
    @JSONField(name = "update_time")
    private String updateTime;

}
