package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 店铺优惠信息
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class ShopDiscountDetail {

    /**
     * 优惠总金额
     */
    @JSONField(name = "total_amount")
    private Integer totalAmount;

    /**
     * 券优惠金额
     */
    @JSONField(name = "coupon_amount")
    private Integer couponAmount;

}
