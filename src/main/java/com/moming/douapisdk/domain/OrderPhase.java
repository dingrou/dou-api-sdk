package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 定金预售阶段单
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class OrderPhase {


    /**
     * 阶段单id
     */
    @JSONField(name = "phase_order_id")
    private String phaseOrderId;

    /**
     * 总共有几阶段
     */
    @JSONField(name = "total_phase")
    private Integer totalPhase;

    /**
     * 第几阶段
     */
    @JSONField(name = "current_phase")
    private Integer currentPhase;

    /**
     * 是否支付成功
     */
    @JSONField(name = "pay_success")
    private Boolean paySuccess;

    /**
     * 商品单ID
     */
    @JSONField(name = "sku_order_id")
    private String skuOrderId;

    /**
     * 活动id
     */
    @JSONField(name = "campaign_id")
    private Long campaignId;

    /**
     * 阶段价格
     * <p/>
     * 定金单价、尾款单价<br/>
     * 单位：分
     */
    @JSONField(name = "phase_payable_price")
    private Integer phasePayablePrice;

    /**
     * 支付类型
     * <p/>
     * 可选值：
     * <blockquote>
     * <ul>
     * <li>0-货到付款</li>
     * <li>1-微信</li>
     * <li>2-支付宝</li>
     * <li>3-小程序</li>
     * <li>4-银行卡</li>
     * <li>5-余额</li>
     * <li>7-无需支付</li>
     * <li>8-放心花(信用支付)</li>
     * <li>9-新卡支付</li>
     * </ul>
     * </blockquote>
     * */
    @JSONField(name = "phase_pay_type")
    private Integer phasePayType;

    /**
     * 阶段单开始开启支付时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "phase_open_time")
    private Long phaseOpenTime;

    /**
     * 阶段单支付成功时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "phase_pay_time")
    private Long phasePayTime;

    /**
     * 阶段单关闭时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "phase_close_time")
    private Long phaseCloseTime;

    /**
     * 渠道支付订单号
     *
     */
    @JSONField(name = "channel_payment_no")
    private String channelPaymentNo;

    /**
     * 阶段单总金额
     * <p/>
     * 单位分<br/>
     *
     * <pre>
     *     为订单金额phase_sum_amount+运费phase_post_amount
     *     为支付金额phase_pay_amount+优惠金额phase_promotion_amount
     * </pre>
     *
     *
     */
    @JSONField(name = "phase_order_amount")
    private Integer phaseOrderAmount;

    /**
     * 阶段单订单金额
     * <p/>
     * 单位分
     */
    @JSONField(name = "phase_sum_amount")
    private Integer phaseSumAmount;

    /**
     * 阶段单运费金额
     * <p/>
     * 单位分
     */
    @JSONField(name = "phase_post_amount")
    private Integer phasePostAmount;

    /**
     * 阶段单用户实际支付金额
     * <p/>
     * 单位分
     */
    @JSONField(name = "phase_pay_amount")
    private Integer phasePayAmount;

    /**
     * 阶段单总优惠金额
     * <p/>
     * 单位分
     */
    @JSONField(name = "phase_promotion_amount")
    private Integer phasePromotionAmount;

    /**
     * 阶段状态描述
     * <p/>
     * Example: 已开始但未支付
     */
    @JSONField(name = "current_phase_status_desc")
    private String currentPhaseStatusDesc;

    /**
     * sku单价
     * <p/>
     * 单位：分
     */
    @JSONField(name = "sku_price")
    private Integer skuPrice;



}
