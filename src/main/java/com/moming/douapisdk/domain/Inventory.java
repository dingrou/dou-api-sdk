package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 库存类型
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class Inventory {


    /**
     * 库存类型
     * 普通库存/区域库存
     */
    @JSONField(name = "inventory_type")
    private Integer inventoryType;

    /**
     * 库存类型描述
     */
    @JSONField(name = "inventory_type_desc")
    private String inventoryTypeDesc;

    /**
     * 外部仓id
     */
    @JSONField(name = "out_warehouse_id")
    private String outWarehouseId;

    /**
     * 仓id
     */
    @JSONField(name = "warehouse_id")
    private String warehouseId;

}
