package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 业务错误
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class CustomErr {

    /**
     * 错误码
     */
    @JSONField(name = "err_code")
    private Integer errCode;


    /**
     * 错误描述
     */
    @JSONField(name = "err_msg")
    private String errMsg;
}
