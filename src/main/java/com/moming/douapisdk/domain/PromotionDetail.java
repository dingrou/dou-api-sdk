package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 优惠信息
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class PromotionDetail {

    /**
     * 店铺优惠信息
     */
    @JSONField(name = "shop_discount_detail")
    private ShopDiscountDetail shopDiscountDetail;

}
