package com.moming.douapisdk;

import java.util.Map;

/**
 * Api请求接口
 *
 * @author tianzong
 * @date 2020/7/23
 */
public interface DouYinRequest <T extends BaseDouYinResponse> {

    /**
     * 返回的数据中data是否是一个数组
     * @return boolean
     */
    boolean isArrayResponseData();

    /**
     * 获取api的url地址
     * @return url地址
     */
    String getApiUrl();

    /**
     * 获取API的名称
     * @return API名称
     */
    String getApiMethodName();

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     * @return 文本请求参数集合
     */
    Map<String, String> getTextParams();

    /**
     * 获取具体响应实现类的定义。
     * @return 获取具体响应实现类的定义。
     */
    Class<T> getResponseClass();
}
