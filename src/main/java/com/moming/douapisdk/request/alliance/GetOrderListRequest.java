package com.moming.douapisdk.request.alliance;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.alliance.GetOrderListResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * 查询联盟订单明细
 *
 * @author lujingpo
 * @date 2021/12/13
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GetOrderListRequest extends BaseDouYinRequest<GetOrderListResponse> {

    /**
     * 订单id列表
     */
    private List<String> orderIds;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/alliance/getOrderList";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "alliance.getOrderList";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();

        if (null != orderIds) {
            map.put("order_ids", JSON.toJSONString(orderIds));
        }
        return map;
    }
}
