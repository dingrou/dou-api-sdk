package com.moming.douapisdk.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.domain.IRequestParameterEnum;
import com.moming.douapisdk.response.CommentListResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 评价列表
 *
 * @author lujingpo
 * @date 2021/2/3
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CommentListRequest extends BaseDouYinRequest<CommentListResponse> {

    /**
     * 创建时间排序
     */
    public final static String ORDER_BY_CREATE_TIME = "create_time";

    /**
     * 更新时间排序
     */
    public final static String ORDER_BY_UPDATE_TIME = "update_time";

    /**
     * 商品id
     * 3539925204033339668
     */
    @JSONField(name = "product_id")
    private String productId;

    /**
     * 订单号，可以是子订单id号
     *
     * 6496679971677798670
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 评价开始时间。如果只输入开始时间，那么能返回开始时间之后的评价数据。
     *
     * 2011-01-01 00:00:00
     */
    @JSONField(name = "start_time")
    private LocalDateTime startTime;

    /**
     * 评价结束时间。如果只输入结束时间，那么全部返回所有评价数据。
     *
     * 2011-01-01 00:00:00
     */
    @JSONField(name = "end_time")
    private LocalDateTime endTime;

    /**
     * create_time或者update_time，默认create_time
     */
    @JSONField(name = "order_by")
    private String orderBy;

    /**
     *
     */
    @JSONField(name = "is_desc")
    private String isDesc;

    /**
     * 评价等级，好评/中评/差评
     *
     * 1好评 2中评 3差评
     * {@link RATE}
     */
    private RATE rate;

    /**
     * 商家回复状态
     */
    @JSONField(name = "is_reply")
    private String isReply;

    /**
     * 页数，第1页从0开始
     */
    private String page;

    /**
     * 每页条数，最大100
     */
    private String size;





    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/comment/list";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "comment.list";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap hashMap = new DouYinHashMap();
        hashMap.put("order_id", orderId);
        hashMap.put("start_time", startTime);
        hashMap.put("end_time", endTime);
        hashMap.put("order_by", orderBy);
        hashMap.put("is_desc", isDesc);
        hashMap.put("rate", rate);
        hashMap.put("is_reply", isReply);
        hashMap.put("page", page);
        hashMap.put("size", size);
        return hashMap;
    }

    /**
     * 获取具体响应实现类的定义。
     *
     * @return 获取具体响应实现类的定义。
     */
    @Override
    public Class<CommentListResponse> getResponseClass() {
        return CommentListResponse.class;
    }


    /**
     * 评价
     */
    public enum RATE implements IRequestParameterEnum<String> {
        /**
         * 差评
         */
        BAD("1"),
        /**
         * 中评
         */
        NORMAL("2"),
        /**
         * 好评
         */
        GOOD("3");

        RATE(String value) {
            this.value = value;
        }

        private final String value;
        /**
         * 枚举参数必须具有该返回值
         *
         * @return 结果
         */
        @Override
        public String getValue() {
            return value;
        }
    }
}
