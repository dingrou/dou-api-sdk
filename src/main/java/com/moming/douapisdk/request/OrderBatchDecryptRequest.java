package com.moming.douapisdk.request;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.CipherInfo;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderBatchDecryptResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * 批量解密
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchDecryptRequest extends BaseDouYinRequest<OrderBatchDecryptResponse> {

    /**
     * 待解密的密文列表
     */
    private List<CipherInfo> cipherInfoList;


    @Override
    public String getApiUrl() {
        return "/order/batchDecrypt";
    }

    @Override
    public String getApiMethodName() {
        return "order.batchDecrypt";
    }

    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();

        if (null != cipherInfoList) {
            map.put("cipher_infos", JSON.toJSONString(cipherInfoList));
        }

        return map;
    }
}
