package com.moming.douapisdk.request;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderDetailNewResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 订单详情查询
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderDetailNewRequest extends BaseDouYinRequest<OrderDetailNewResponse> {

    /**
     * 店铺订单号
     */
    private String shopOrderId;

    /**
     * 密文是否可搜索
     */
    private Boolean isSearchable;


    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/order/orderDetail";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "order.orderDetail";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap hashMap = new DouYinHashMap();
        hashMap.put("shop_order_id", this.shopOrderId);
        hashMap.put("is_searchable", this.isSearchable);
        return hashMap;
    }
}
