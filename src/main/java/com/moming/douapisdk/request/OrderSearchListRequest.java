package com.moming.douapisdk.request;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.CombineStatus;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderSearchListResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 根据条件检索满足要求的订单列表，支持下单时间和更新时间排序
 *
 * @author lujingpo
 * @date 2021/6/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderSearchListRequest extends BaseDouYinRequest<OrderSearchListResponse> {
    /**
     * 按创建时间排序
     */
    public final static String ORDER_BY_FIELD_CREATE = "create_time";

    /**
     * 按更新时间排序
     */
    public final static String ORDER_BY_FIELD_UPDATE = "update_time";

    /**
     * 商品名称
     */
    private String product;


    /**
     * 售后状态：
     * all-全部
     * in_aftersale-售后中
     * refund-退款中
     * refund_success-退款成功
     * refund_fail-退款失败
     * exchange_success-换货成功
     * aftersale_close-售后关闭
     */
    private String afterSaleStatusDesc;

    /**
     * 物流单号
     */
    private String trackingNo;

    /**
     * 状态组合查询，直接输入状态码
     */
    private List<CombineStatus> combineStatus;

    /**
     * 排序条件
     * <blockquote>
     *     可选值为:
     *     <ul>
     *         <li>创建时间:{@link #ORDER_BY_FIELD_CREATE}</li>
     *         <li>更新时间:{@link #ORDER_BY_FIELD_UPDATE}</li>
     *     </ul>
     * </blockquote>
     */
    private String orderBy;
    /**
     * app渠道:0 站外 1 火山 2 抖音 3 头条 4 西瓜 5 微信 6 值点app
     * 7 头条lite 8 懂车帝 9 皮皮虾 11 抖音极速版 12 TikTok
     * 13 musically 14 穿山甲 15 火山极速版 16 服务市场
     */
    private Integer bType;
    /**
     *  预售类型：1 全款预售
     */
    private Integer presellType;
    /**
     *  订类型:0-普通订单 2-虚拟订单 4-平台券码 5-商家券码
     */
    private Integer orderType;
    /**
     *  下单时间：开始
     *  秒级时间戳
     */
    private Long createTimeStart;
    /**
     *  下单时间：截止
     *  秒级时间戳
     */
    private Long createTimeEnd;
    /**
     *  异常订单，1-异常取消，2-风控审核中
     */
    private Integer abnormalOrder;
    /**
     *  交易类型，1 拼团订单，2 定金预售
     */
    private Integer tradeType;
    /**
     *  更新时间：开始
     *  秒级时间戳
     */
    private Long updateTimeStart;
    /**
     *  更新时间：截止
     *  秒级时间戳
     */
    private Long updateTimeEnd;

    /**
     * 排序类型，小到大或大到小，默认大到小
     * 默认：false
     */
    private Boolean orderAsc;

    /**
     * 单页大小，限制100以内
     */
    private Integer size;
    /**
     * 页码，0页开始
     */
    private Integer page;

    /**
     * 密文是否可搜索
     */
    private Boolean isSearchable;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/order/searchList";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "order.searchList";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        map.put("product", product);
        map.put("after_sale_status_desc", afterSaleStatusDesc);
        map.put("tracking_no", trackingNo);
        if (null != combineStatus) {
            String combineStatusStr = JSON.toJSONString(combineStatus);
            map.put("combine_status", combineStatusStr);
        }
        map.put("order_by", orderBy);
        map.put("b_type", bType);
        map.put("presell_type", presellType);
        map.put("order_type", orderType);
        map.put("create_time_start", createTimeStart);
        map.put("create_time_end", createTimeEnd);
        map.put("abnormal_order", abnormalOrder);
        map.put("trade_type", tradeType);
        map.put("update_time_start", updateTimeStart);
        map.put("update_time_end", updateTimeEnd);
        map.put("order_asc", orderAsc);
        map.put("size", size);
        map.put("page", page);
        map.put("is_searchable", isSearchable);
        return map;
    }
}
