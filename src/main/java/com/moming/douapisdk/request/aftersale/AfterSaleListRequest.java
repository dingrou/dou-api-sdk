package com.moming.douapisdk.request.aftersale;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.internal.util.TimeUtils;
import com.moming.douapisdk.response.aftersale.AfterSaleListResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 售后列表接口
 *
 * 售后列表接口，可以支持多种筛选规则，时间均为以秒为单位的时间戳，金额均以分为单位。
 * 接口中page*size最多不能超过50,000条数据。如需查询更多，请增加条件筛选后查询。
 *
 * @author lujingpo
 * @date 2022/1/25
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AfterSaleListRequest extends BaseDouYinRequest<AfterSaleListResponse> {

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 售后类型，枚举为0(退货退款),1(已发货仅退款),2(未发货仅退款),3(换货), 6(价保)
     *
     */
    private Long afterSaleType;

    /**
     * 售后理由分类，枚举为1(七天无理由退货),2(非七天无理由退货)
     */
    private Long reason;

    /**
     * 退货物流状态，枚举为1(全部),2(已发货),3(未发货)
     */
    private Long logisticsStatus;

    /**
     * 付款方式，枚举为1(全部), 2(货到付款),3(线上付款)
     */
    private Long payType;

    /**
     * 退款类型，枚举为0(原路退款),1(线下退款),2(备用金),3(保证金),4(无需退款)
     */
    private Long refundType;

    /**
     * 仲裁状态，枚举为0(未介入),1(客服处理中),2(仲裁结束-支持买家),
     * 3(仲裁结束-支持卖家),4(待商家举证),5(待与买家协商),6(仲裁结束),255(取消)
     */
    private Long arbitrateStatus;

    /**
     * 订单插旗
     */
    private List<Long> orderFlag;

    /**
     * 申请时间开始，单位为秒
     */
    private LocalDateTime startTime;

    /**
     * 申请时间结束，单位为秒
     */
    private LocalDateTime endTime;

    /**
     * 金额下限，单位为分
     */
    private Long amountStart;

    /**
     * 金额上限，单位为分
     */
    private Long amountEnd;

    /**
     * 风控标签，枚举为-1(退货正常),1(退货异常)
     */
    private Long riskFlag;

    /**
     * 排序方式，优先级按照列表顺序从前往后依次减小，
     * 写法为"<字段名称> <排序方式>"，
     * 字段名称目前支持:
     * "status_deadline"（逾期时间）、
     * "apply_time"（申请时间）
     * "update_time"（更新时间）
     *
     * 排序方式目前支持:
     * "asc"（升序）
     * "desc"（降序）。
     * 按照"逾期时间"升序排列，会优先返回临近逾期时间的数据。
     * example:
     * ["status_deadline desc"]
     */
    private List<String> orderBy;

    /**
     * 页数，从0开始
     */
    private Long page;

    /**
     * 每页数量，最多100个
     */
    private Long size;

    /**
     * 售后ID
     */
    private String afterSaleId;

    /**
     * 售后状态
     *
     * 枚举为
     * 2(发货前退款待处理),
     * 3(发货后仅退款待处理),
     * 4(退货待处理),
     * 5(换货待处理),
     * 6(同意退款，退款失败),
     * 7(同意退款，退款成功),
     * 8(待商家处理),
     * 9(待商家收货),
     * 10(同意退款，退款中),
     * 11(仲裁中),
     * 12(售后关闭),
     * 13(待买家退货),
     * 14(等待用户收货),
     * 15(换货成功),
     * 16(拒绝售后),
     * 17(退货后拒绝退款),
     * 18(待商家上传凭证),
     * 19(仲裁中-待商家举证),
     * 20(仲裁中-待商家举证或协商期)
     */
    private List<Long> standardAfterSaleStatus;

    /**
     * 是否展示特殊售后
     */
    private Boolean needSpecialType;

    /**
     * 更新时间开始，单位为秒
     */
    private LocalDateTime updateStartTime;

    /**
     * 更新时间结束，单位为秒
     */
    private LocalDateTime updateEndTime;

    /**
     * 正向物流单号
     */
    private List<String> orderLogisticsTrackingNo;

    /**
     * 正向物流状态（仅支持拒签场景下的状态筛选，状态更新有一定时延。
     * 1：买家已拒签；
     * 2：买家已签收；
     * 3：快递退回中，运往商家，包含快递拦截成功；
     * 4：商家已签收）
     */
    private List<Long> orderLogisticsState;

    /**
     * 是否拒签后退款（1：已同意拒签, 2：未同意拒签）
     */
    private List<Long> agreeRefuseSign;



    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/afterSale/List";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "afterSale.List";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap map = new DouYinHashMap();
        map.put("order_id", orderId);
        map.put("aftersale_type", afterSaleType);
        map.put("reason", reason);
        map.put("logistics_status", logisticsStatus);
        map.put("pay_type", payType);
        map.put("refund_type", refundType);
        map.put("arbitrate_status", arbitrateStatus);
        if (null != orderFlag && orderFlag.size() > 0) {
            map.put("order_flag", JSON.toJSONString(orderFlag));
        }
        if (null != startTime) {
            map.put("start_time", TimeUtils.toEpochSecond(startTime));
        }
        if (null != endTime) {
            map.put("end_time", TimeUtils.toEpochSecond(endTime));
        }
        map.put("amount_start", amountStart);
        map.put("amount_end", amountEnd);
        map.put("risk_flag", riskFlag);
        if (null != orderBy && orderBy.size() > 0) {
            map.put("order_by", JSON.toJSONString(orderBy));
        }
        map.put("page", page);
        map.put("size", size);
        map.put("aftersale_id", afterSaleId);
        if (null != standardAfterSaleStatus && standardAfterSaleStatus.size() > 0) {
            map.put("standard_aftersale_status", JSON.toJSONString(standardAfterSaleStatus));
        }
        if (null != needSpecialType) {
            map.put("need_special_type", JSON.toJSONString(needSpecialType));
        }
        if (null != updateStartTime) {
            map.put("update_start_time", TimeUtils.toEpochSecond(updateStartTime));
        }
        if (null != updateEndTime) {
            map.put("update_end_time", TimeUtils.toEpochSecond(updateEndTime));
        }
        if (null != orderLogisticsTrackingNo && orderLogisticsTrackingNo.size() > 0) {
            map.put("order_logistics_tracking_no", JSON.toJSONString(orderLogisticsTrackingNo));
        }
        if (null != orderLogisticsState && orderLogisticsState.size() > 0) {
            map.put("order_logistics_state", JSON.toJSONString(orderLogisticsState));
        }
        if (null != agreeRefuseSign && agreeRefuseSign.size() > 0) {
            map.put("agree_refuse_sign", JSON.toJSONString(agreeRefuseSign));
        }
        return map;
    }
}
