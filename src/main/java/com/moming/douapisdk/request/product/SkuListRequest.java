package com.moming.douapisdk.request.product;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.product.SkuListResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 根据商品id获取商品的sku列表，支持返回预占库存信息
 *
 * @author lujingpo
 * @date 2022/3/22
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SkuListRequest extends BaseDouYinRequest<SkuListResponse> {

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 外部商品id
     */
    private Long outProductId;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/sku/list";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "sku.list";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        map.put("product_id", this.productId);
        map.put("out_product_id", this.outProductId);
        return map;
    }

    /**
     * 返回的数据中data是否是一个数组
     *
     * <pre>
     *     其他的接口返回的data都是一个对象，可以直接解析到response对象上面，只有
     *     物流公司这个，data直接是一个数组，特殊返回特殊处理
     *     参考链接 <a href="https://op.jinritemai.com/docs/api-docs/16/76">物流调用api</a>
     * </pre>
     *
     * @return boolean
     */
    @Override
    public boolean isArrayResponseData() {
        return true;
    }
}
