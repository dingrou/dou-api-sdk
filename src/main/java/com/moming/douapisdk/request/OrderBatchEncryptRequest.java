package com.moming.douapisdk.request;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.BatchEncrypt;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderBatchEncryptResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * 批量加密接口
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchEncryptRequest extends BaseDouYinRequest<OrderBatchEncryptResponse> {

    /**
     * 待加密列表
     */
    private List<BatchEncrypt> batchEncryptList;


    @Override
    public String getApiUrl() {
        return "/order/batchEncrypt";
    }

    @Override
    public String getApiMethodName() {
        return "order.batchEncrypt";
    }

    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        if (null != batchEncryptList) {
            map.put("batch_encrypt_list", JSON.toJSONString(batchEncryptList));
        }
        return map;
    }
}
