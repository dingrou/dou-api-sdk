package com.moming.douapisdk.internal.util;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * 时间工具类
 *
 * @author lujingpo
 * @date 2022/1/20
 */
public class TimeUtils {

    public static long toEpochSecond(LocalDateTime time) {
        return time.atZone(ZoneId.systemDefault())
                .toEpochSecond();
    }

}
