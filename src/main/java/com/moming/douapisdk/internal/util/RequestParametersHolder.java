package com.moming.douapisdk.internal.util;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.ToString;

import java.util.TreeMap;

/**
 * 请求参数 存储结构体
 *
 * 存放请求url，请求参数，返回值
 *
 * @author tianzong
 * @date 2020/7/21
 */
@Data
@ToString
public class RequestParametersHolder {

    private String requestUrl;
    private String responseBody;

    private DouYinHashMap protocolParams;
    private DouYinHashMap paramJson;


    public String getParamJson() {
        return JSONObject.toJSONString(new TreeMap<>(this.paramJson));
    }

}
