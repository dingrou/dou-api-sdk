package com.moming.douapisdk.response.alliance;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 联盟订单明细
 * @author lujingpo
 * @date 2021/12/13
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GetOrderListResponse extends BaseDouYinResponse {

    /**
     * 返回编码
     */
    private String code;

    /**
     * 返回信息
     */
    @JSONField(name = "code_msg")
    private String codeMsg;

    /**
     * 订单明细数据
     */
    private List<Datas> datas;

    @Data
    public static class Datas {

        /**
         * 订单id
         */
        @JSONField(name = "order_id")
        private String orderId;

        /**
         * 产品id
         */
        @JSONField(name = "product_id")
        private String productId;

        /**
         * 达人账户
         */
        @JSONField(name = "author_account")
        private String authorAccount;

        /**
         * 达人short_id
         */
        @JSONField(name = "short_id")
        private String shortId;

        /**
         * 支付金额，单位：分
         */
        @JSONField(name = "total_pay_amount")
        private Long totalPayAmount;

        /**
         * 佣金率，以万为基数，比如3000，就是30%
         */
        @JSONField(name = "commission_rate")
        private Long commissionRate;

        /**
         * 预估佣金，单位：分
         */
        @JSONField(name = "estimated_comission")
        private Long estimatedComission;

        /**
         * 真实佣金，单位：分
         */
        @JSONField(name = "real_comission")
        private Long realComission;

        /**
         * 订单状态：支付成功、确认收货、退款、结算
         */
        @JSONField(name = "order_status")
        private String orderStatus;

        /**
         * 店铺id
         */
        @JSONField(name = "shop_id")
        private String shopId;

        /**
         * 业务类型：COMMON普通单、PRE_SELL预售
         */
        @JSONField(name = "alliance_biz_type")
        private String allianceBizType;

        /**
         * 阶段单号：1定金、2尾款
         */
        @JSONField(name = "phase_id")
        private Integer phaseId;
    }
}
