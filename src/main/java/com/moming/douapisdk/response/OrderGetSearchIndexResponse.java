package com.moming.douapisdk.response;

import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 索引串
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderGetSearchIndexResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = -404523134725572348L;


    /**
     * 订单加密索引串
     */
    private String encryptIndexText;

}
