package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 评价列表
 *
 * @author lujingpo
 * @date 2021/2/3
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CommentListResponse extends BaseDouYinResponse {

    private static final long serialVersionUID = -680584607914589521L;

    /**
     * 评论列表
     */
    private List<Comments> comments;

    /**
     * 条数
     */
    private Integer total;

    /**
     * 评论
     */
    @Data
    public static class Comments {

        /**
         * 追加的评价内容
         */
        @JSONField(name = "append_content")
        private String appendContent;

        /**
         * 追加的评价图片
         */
        @JSONField(name = "append_photos")
        private List<AppendPhotos> appendPhotos;

        /**
         * 追加评价时间
         *
         * example: 2011-01-01 00:00:00
         */
        @JSONField(name = "append_time")
        private String appendTime;

        /**
         * 评价id
         */
        @JSONField(name = "comment_id")
        private Long commentId;

        /**
         * 评价时间
         *
         * example: 2020-08-27 20:29:20
         */
        @JSONField(name = "comment_time")
        private String commentTime;

        /**
         * 评价内容
         */
        private String content;

        /**
         * 是否已修改的评价
         */
        @JSONField(name = "is_changed")
        private boolean isChanged;

        /**
         * 商家回复状态
         */
        @JSONField(name = "is_reply")
        private boolean isReply;

        /**
         * 子订单号
         */
        @JSONField(name = "order_id")
        private Long orderId;

        /**
         * 评价图片
         */
        private List<String> photos;

        /**
         * 商品id
         */
        @JSONField(name = "product_id")
        private Long productId;

        /**
         * 商品名称
         */
        @JSONField(name = "product_name")
        private String productName;

        /**
         * 评价等级，好评/中评/差评
         *
         * example 1好评，2中评，3差评
         */
        private Integer rate;

        /**
         * 卖家回复的评论
         */
        @JSONField(name = "reply_content")
        private String replyContent;

        /**
         * 评价更新时间
         *
         * example: 2011-01-01 00:00:00
         */
        @JSONField(name = "update_time")
        private String updateTime;
    }

    @Data
    public static class AppendPhotos {

        private String thumbnail;
        private String url;
    }
}
