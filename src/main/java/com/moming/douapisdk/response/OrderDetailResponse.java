package com.moming.douapisdk.response;

import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.Order;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author tianzong
 * @date 2020/7/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderDetailResponse extends BaseDouYinResponse {

    private static final long serialVersionUID = -7698845462133858817L;

    private Long total;

    private Long count;

    private List<Order> list;

}
