package com.moming.douapisdk.response.aftersale;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author lujingpo
 * @date 2022/1/25
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AfterSaleListResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = -253321053632263286L;

    /**
     * 是否还有更多
     */
    @JSONField(name = "has_more")
    private Boolean hasMore;

    /**
     * 售后列表元素
     */
    private List<Item> items;

    /**
     * 页码，从0开始
     */
    private Long page;

    /**
     * 当前返回售后数量
     */
    private Long size;
    /**
     * 当前搜索条件下，匹配到的总数量
     */
    private Long total;

    @Data
    public static class Item {
        @JSONField(name = "aftersale_info")
        private AfterSaleInfo afterSaleInfo;
        @JSONField(name = "order_info")
        private OrderInfo orderInfo;
        @JSONField(name = "seller_logs")
        private List<SellerLogs> sellerLogs;
        @JSONField(name = "text_part")
        private TextPart textPart;
    }

    /**
     * 售后信息
     */
    @Data
    public static class AfterSaleInfo {
        /**
         * 售后ID
         */
        @JSONField(name = "aftersale_id")
        private String afterSaleId;
        /**
         * 售后数量
         */
        @JSONField(name = "aftersale_num")
        private Long afterSaleNum;
        /**
         * 售后订单类型，枚举为-1(历史订单),1(商品单),2(店铺单)
         */
        @JSONField(name = "aftersale_order_type")
        private Long afterSaleOrderType;
        /**
         * 售后退款类型，枚举为
         * -1(历史数据默认值),
         * 0(订单货款/原路退款),
         * 1(货到付款线下退款),
         * 2(备用金),
         * 3(保证金),
         * 4(无需退款),
         * 5(平台垫付)
         */
        @JSONField(name = "aftersale_refund_type")
        private Long afterSaleRefundType;
        /**
         * 售后状态，枚举为
         * 6(待商家同意),
         * 7(待买家退货),
         * 11(待商家二次同意),
         * 12(售后成功),
         * 13(换货待买家收货),
         * 14(换货成功),
         * 27(商家一次拒绝),
         * 28(售后失败),
         * 29(商家二次拒绝)
         */
        @JSONField(name = "aftersale_status")
        private String afterSaleStatus;
        /**
         * 售后类型，枚举为
         * 0(退货退款),
         * 1(已发货仅退款),
         * 2(未发货仅退款),
         * 3(换货)
         */
        @JSONField(name = "aftersale_type")
        private String afterSaleType;
        /**
         * 申请时间
         */
        @JSONField(name = "apply_time")
        private Long applyTime;
        /**
         * 仲裁责任方
         */
        @JSONField(name = "arbitrate_blame")
        private Long arbitrateBlame;
        /**
         * 仲裁状态，枚举为
         * 0(无仲裁记录),
         * 1(仲裁中),
         * 2(客服同意),
         * 3(客服拒绝),
         * 4(待商家举证),
         * 5(协商期),
         * 255(仲裁结束)
         */
        @JSONField(name = "arbitrate_status")
        private Long arbitrateStatus;
        /**
         * 售后单创建时间
         */
        @JSONField(name = "create_time")
        private Long createTime;
        /**
         * 换货物流公司名称
         */
        @JSONField(name = "exchange_logistics_company_name")
        private String exchangeLogisticsCompanyName;
        /**
         * 换货SKU信息
         */
        @JSONField(name = "exchange_sku_info")
        private ExchangeSkuInfo exchangeSkuInfo;
        /**
         * 买家是否收到货物，
         * 0表示未收到，
         * 1表示收到
         */
        @JSONField(name = "got_pkg")
        private Long gotPkg;

        /**
         * 是否拒签后退款（1：已同意拒签, 2：未同意拒签）
         */
        @JSONField(name = "is_agree_refuse_sign")
        private Long isAgreeRefuseSign;
        /**
         * 商家剩余发送短信（催用户寄回）次数
         */
        @JSONField(name = "left_urge_sms_count")
        private Long leftUrgeSmsCount;
        /**
         * 商家首次发货的正向物流信息
         */
        @JSONField(name = "order_logistics")
        private List<OrderLogistics> orderLogistics;
        /**
         * 部分退类型
         */
        @JSONField(name = "part_type")
        private Long partType;
        /**
         * 用户申请售后时选择的二级原因标签
         */
        @JSONField(name = "reason_second_labels")
        private List<ReasonSecondLabels> reasonSecondLabels;

        /**
         * 售后退款金额，单位为分
         */
        @JSONField(name = "refund_amount")
        private Long refundAmount;
        /**
         * 售后退运费金额，单位为分
         */
        @JSONField(name = "refund_post_amount")
        private Long refundPostAmount;
        /**
         * 退款状态
         */
        @JSONField(name = "refund_status")
        private Long refundStatus;
        /**
         * 退税费
         */
        @JSONField(name = "refund_tax_amount")
        private String refundTaxAmount;
        /**
         * 退款方式，枚举为
         * 1(极速退款助手)、
         * 2(售后小助手)、
         * 3(售后急速退)、
         * 4(闪电退货)
         */
        @JSONField(name = "refund_type")
        private Long refundType;
        /**
         * 关联的订单ID
         */
        @JSONField(name = "related_id")
        private String relatedId;
        /**
         * 售后商家备注
         */
        private String remark;

        /**
         * 退货物流单号
         */
        @JSONField(name = "return_logistics_code")
        private String returnLogisticsCode;
        /**
         * 退货物流公司名称
         */
        @JSONField(name = "return_logistics_company_name")
        private String returnLogisticsCompanyName;
        /**
         * 退优惠金额
         */
        @JSONField(name = "return_promotion_amount")
        private Long returnPromotionAmount;
        /**
         * 风控码
         */
        @JSONField(name = "risk_decision_code")
        private Long riskDecisionCode;
        /**
         * 风控描述
         */
        @JSONField(name = "risk_decision_description")
        private String riskDecisionDescription;
        /**
         * 风控理由
         */
        @JSONField(name = "risk_decision_reason")
        private String riskDecisionReason;
        /**
         * 当前节点逾期时间
         */
        @JSONField(name = "status_deadline")
        private Long statusDeadline;
        /**
         * 最近更新时间
         */
        @JSONField(name = "update_time")
        private Long updateTime;
    }

    /**
     * 文案部分
     */
    @Data
    public static class TextPart {
        /**
         * 售后退款类型文案
         */
        @JSONField(name = "aftersale_refund_type_text")
        private String afterSaleRefundTypeText;
        /**
         * 售后状态文案
         */
        @JSONField(name = "aftersale_status_text")
        private String afterSaleStatusText;
        /**
         * 售后类型文案
         */
        @JSONField(name = "aftersale_type_text")
        private String afterSaleTypeText;
        /**
         * 仲裁状态文案
         */
        @JSONField(name = "arbitrate_status_text")
        private String arbitrateStatusText;
        /**
         * 坏单比例文案
         */
        @JSONField(name = "bad_item_text")
        private String badItemText;
        /**
         * 正向物流发货状态文案
         */
        @JSONField(name = "logistics_text")
        private String logisticsText;
        /**
         * 售后理由文案
         */
        @JSONField(name = "reason_text")
        private String reasonText;

        /**
         * 退货物流发货状态文案
         */
        @JSONField(name = "return_logistics_text")
        private String returnLogisticsText;
    }

    /**
     * 卖家插旗日志
     */
    @Data
    public static class SellerLogs {

        /**
         * 插旗日志内容
         */
        private String content;
        /**
         * 插旗时间（字符串格式）
         */
        @JSONField(name = "create_time")
        private String createTime;
        /**
         * 插旗操作人
         */
        @JSONField(name ="op_name")
        private String opName;
        /**
         * 插旗类型
         */
        private Long star;

    }

    /**
     * 订单信息
     */
    @Data
    public static class OrderInfo {
        /**
         * 订单插旗
         */
        @JSONField(name = "order_flag")
        private Long orderFlag;
        /**
         * 售后关联的订单信息
         */
        @JSONField(name = "related_order_info")
        private List<RelatedOrderInfo> relatedOrderInfo;
        /**
         * 店铺单订单ID
         */
        @JSONField(name = "shop_order_id")
        private String shopOrderId;
    }

    /**
     * 售后关联的订单信息
     */
    @Data
    public static class RelatedOrderInfo {
        /**
         * 售后商品数量
         */
        @JSONField(name = "aftersale_item_num")
        private Long afterSaleItemNum;

        /**
         * 优惠券金额
         */
        @JSONField(name = "aftersale_pay_amount")
        private Long afterSalePayAmount;

        /**
         * 售后退运费金额
         */
        @JSONField(name = "aftersale_post_amount")
        private String afterSalePostAmount;

        /**
         * 售后退税费金额
         */
        @JSONField(name = "aftersale_tax_amount")
        private String afterSaleTaxAmount;

        /**
         * 下单时间
         */
        @JSONField(name = "create_time")
        private Long createTime;

        /**
         * 赠品订单id列表
         */
        @JSONField(name = "given_sku_order_ids")
        private List<String> givenSkuOrderIds;

        /**
         * 是否为海外订单
         */
        @JSONField(name = "is_oversea_order")
        private Long isOverseaOrder;
        /**
         * 购买数量
         */
        @JSONField(name = "item_num")
        private Long itemNum;
        /**
         * 发货物流编码
         */
        @JSONField(name = "logistics_code")
        private String logisticsCode;
        /**
         * 正向物流公司名称
         */
        @JSONField(name = "logistics_company_name")
        private String logisticsCompanyName;
        /**
         * 订单状态，枚举为
         * 2(未发货),
         * 3(已发货),
         * 5(已收货或已完成),
         * 255(已完成)
         */
        @JSONField(name = "order_status")
        private Long orderStatus;
        /**
         * 付款金额
         */
        @JSONField(name = "pay_amount")
        private Long payAmount;
        /**
         * 付运费金额
         */
        @JSONField(name = "post_amount")
        private Long postAmount;
        /**
         * 价格
         */
        private Long price;
        /**
         * 商品ID
         */
        @JSONField(name = "product_id")
        private Long productId;
        /**
         * 商品图片
         */
        @JSONField(name = "product_image")
        private String productImage;
        /**
         * 商品名称
         */
        @JSONField(name = "product_name")
        private String productName;

        /**
         * 优惠券金额
         */
        @JSONField(name = "promotion_pay_amount")
        private Long promotionPayAmount;
        /**
         * 商家SKU编码
         */
        @JSONField(name = "shop_sku_code")
        private String shopSkuCode;
        /**
         * 商品单信息
         */
        @JSONField(name = "sku_order_id")
        private String skuOrderId;
        /**
         * 商品规格
         */
        @JSONField(name = "sku_spec")
        private List<SkuSpec> skuSpec;

        /**
         * 订单标签
         */
        private List<Tags> tags;
        /**
         * 税费
         */
        @JSONField(name = "tax_amount")
        private String taxAmount;
    }

    @Data
    public static class SkuSpec {
        /**
         * 规格类型名称
         */
        private String name;
        /**
         * 规格值
         */
        private String value;
    }

    @Data
    public static class Tags {
        /**
         * 标签中文名称
         */
        @JSONField(name = "tag_detail")
        private String tagDetail;
        /**
         * 标签编号
         */
        @JSONField(name = "tag_detail_en")
        private String tagDetailEn;
        /**
         * 标签链接
         */
        @JSONField(name = "tag_link_url")
        private String tagLinkUrl;
    }

    /**
     * 换货SKU信息
     */
    @Data
    public static class ExchangeSkuInfo {
        /**
         * 换货SKU code
         */
        private String code;
        /**
         * 商品名称
         */
        private String name;
        /**
         * 换货数目
         */
        private String num;
        /**
         * 商家编号
         */
        @JSONField(name = "out_sku_id")
        private String outSkuId;
        /**
         * 区域库存仓ID
         */
        @JSONField(name = "out_warehouse_id")
        private String outWarehouseId;
        /**
         * 换货商品的价格，单位分
         */
        private String price;
        /**
         * 换货SkuID
         */
        @JSONField(name = "sku_id")
        private String skuId;
        /**
         * sku规格信息
         */
        @JSONField(name = "spec_desc")
        private String specDesc;
        /**
         * sku外部供应商编码供应商ID
         */
        @JSONField(name = "supplier_id")
        private String supplierId;
        /**
         * 商品图片url
         */
        private String url;
    }

    /**
     * 用户申请售后时选择的二级原因标签
     */
    @Data
    public static class ReasonSecondLabels {
        /**
         * 二级原因标签编号
         */
        private Long code;
        /**
         * 二级原因标签名称
         */
        private String name;
    }

    /**
     * 商家首次发货的正向物流信息
     */
    @Data
    public static class OrderLogistics {
        /**
         * 物流公司编码
         */
        @JSONField(name = "company_code")
        private String companyCode;
        /**
         * 物流公司名称
         */
        @JSONField(name = "company_name")
        private String companyName;
        /**
         * 正向物流状态
         */
        @JSONField(name = "logistics_state")
        private Long logisticsState;
        /**
         * 物流状态到达时间
         */
        @JSONField(name = "logistics_time")
        private Long logisticsTime;
        /**
         * 物流单号
         */
        @JSONField(name = "tracking_no")
        private String trackingNo;
    }
}
