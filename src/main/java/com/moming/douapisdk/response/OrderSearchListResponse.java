package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 订单检索接口返回值
 *
 * @author lujingpo
 * @date 2021/6/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderSearchListResponse extends BaseDouYinResponse {

    private static final long serialVersionUID = -4045230382515572348L;

    /**
     * 页数，从0开始
     */
    private Long page;
    /**
     * 总订单数
     */
    private Long total;
    /**
     * 单页大小
     */
    private Long size;
    /**
     * 订单信息
     */
    @JSONField(name = "shop_order_list")
    private List<ShopOrder> shopOrderList;

}
