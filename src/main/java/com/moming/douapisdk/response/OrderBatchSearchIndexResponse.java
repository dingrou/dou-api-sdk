package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.CustomErr;
import com.moming.douapisdk.domain.PlainToEncryptIndex;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 索引串
 * @author LangYu
 * @date 2021/7/10
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchSearchIndexResponse extends BaseDouYinResponse {


    /**
     * 明文转化为索引穿列表
     */
    @JSONField(name = "plain_to_encrypt_index_list")
    private List<PlainToEncryptIndex> plainToEncryptIndexList;

    /**
     * 业务错误
     */
    @JSONField(name = "custom_err")
    private CustomErr customErr;
}
