package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.AfterSale;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 售后订单列表查询
 *
 * @author lujingpo
 * @date 2021/2/1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TradeRefundListSearchResponse extends BaseDouYinResponse {


    private static final long serialVersionUID = 9057716152652845184L;

    private Long total;

    @JSONField(name = "aftersale_list")
    private List<AfterSale> afterSaleList;

}
