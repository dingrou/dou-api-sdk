package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 根据子订单ID查询退款详情
 *
 * @author lujingpo
 * @date 2021/3/1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RefundProcessDetailResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = -7246779120717098521L;

    /**
     * 订单信息
     */
    @JSONField(name = "order_info")
    private OrderInfo orderInfo;

    /**
     * 退款的当前实时处理信息
     */
    @JSONField(name = "process_info")
    private ProcessInfo processInfo;

    /**
     * 退款的历史处理日志信息
     */
    private List<Logs> logs;

    /**
     * 退的件数
     */
    @JSONField(name = "refund_total_num")
    private int refundTotalNum;

    /**
     * 退款总金额，单位是分（包含退的运费）
     */
    @JSONField(name = "refund_total_amount")
    private int refundTotalAmount;

    /**
     * 退的运费金额，单位是分
     */
    @JSONField(name = "refund_post_amount")
    private int refundPostAmount;

    /**
     * 当前子订单的售后记录（1次或多次）
     */
    @JSONField(name = "process_info_list")
    private List<ProcessInfoList> processInfoList;

    /**
     * 售后单相关信息
     */
    @JSONField(name = "aftersale_info")
    private AftersaleInfo aftersaleInfo;

    /**
     * 当前子订单的仲裁单记录（最近一次）
     */
    @JSONField(name = "arbitrate_info")
    private ArbitrateInfo arbitrateInfo;

    /**
     * 商家备注
     */
    @JSONField(name = "seller_order_remarks")
    private List<String> sellerOrderRemarks;

    /**
     * 当前售后状态的超时时间
     */
    @JSONField(name = "status_deadline")
    private String statusDeadline;

    /**
     * 订单信息
     */
    @Data
    public static class OrderInfo {

        /**
         * 订单id
         */
        @JSONField(name = "order_id")
        private long orderId;

        /**
         * 订单的pid
         */
        private long pid;

        /**
         * 订单状态
         */
        @JSONField(name = "order_status")
        private int orderStatus;

        /**
         * 订单最后状态
         */
        @JSONField(name = "final_status")
        private int finalStatus;

        /**
         * 订单状态释义
         */
        @JSONField(name = "status_desc")
        private String statusDesc;

        /**
         * 订单创建时间
         */
        @JSONField(name = "create_time")
        private String createTime;

        /**
         * 收货时间。未收货时为"0"，已发货返回秒级时间戳
         */
        @JSONField(name = "receipt_time")
        private String receiptTime;

        /**
         * 该子订单所购买的sku的数量
         */
        @JSONField(name = "combo_num")
        private int comboNum;

        /**
         * 该子订单所购买的sku的售价
         */
        @JSONField(name = "combo_amount")
        private int comboAmount;

        /**
         * 子订单实付金额（不包含运费）
         */
        @JSONField(name = "total_amount")
        private int totalAmount;

        @JSONField(name = "pay_amount")
        private int payAmount;

        /**
         * 店铺ID
         */
        @JSONField(name = "shop_id")
        private int shopId;

        /**
         * 商品id
         */
        @JSONField(name = "product_id")
        private long productId;

        /**
         * 商品名
         */
        @JSONField(name = "product_name")
        private String productName;

        /**
         * 商品主图
         */
        @JSONField(name = "product_img")
        private String productImg;

        /**
         * 当前售后状态的超时时间
         */
        @JSONField(name = "status_deadline")
        private String statusDeadline;

        /**
         * 当前子订单的仲裁单记录（最近一次）
         */
        @JSONField(name = "arbitrate_info")
        private ArbitrateInfo arbitrateInfo;
    }

    @Data
    public static class Logs {

        /**
         * 创建时间
         */
        @JSONField( name = "create_time")
        private String createTime;

        /**
         * 操作
         * 申请退货 / 商家同意退货 /填写退货物流
         */
        private String action;

        /**
         * 内容
         */
        private String desc;

        /**
         * 操作者
         */
        private String operator;

        /**
         * 证据列表
         */
        private List<String> evidence;
    }

    @Data
    public static class ProcessInfo {

        /**
         * 申请售后时间
         */
        @JSONField(name = "apply_time")
        private String applyTime;
        private String reason;
        @JSONField(name = "type_desc")
        private String typeDesc;
        @JSONField(name = "apply_remark")
        private String applyRemark;
        private List<String> evidence;
        @JSONField(name = "logistics_time")
        private String logisticsTime;
        @JSONField(name = "logistics_code")
        private String logisticsCode;
        @JSONField(name = "logistics_name")
        private String logisticsName;
        @JSONField(name = "aftersale_id")
        private long aftersaleId;
        @JSONField(name = "aftersale_status")
        private int aftersaleStatus;
        @JSONField(name = "refund_type")
        private int refundType;
        @JSONField(name = "refund_status")
        private int refundStatus;
        @JSONField(name = "post_receiver")
        private String postReceiver;
        @JSONField(name = "post_tel")
        private String postTel;
        @JSONField(name = "post_addr")
        private String postAddr;
        @JSONField(name = "exchange_logistics_code")
        private String exchangeLogisticsCode;
        @JSONField(name = "exchange_logistics_name")
        private String exchangeLogisticsName;
        @JSONField(name = "expire_time")
        private String expireTime;
    }

    @Data
    public static class ProcessInfoList {
        @JSONField(name = "apply_time")
        private String applyTime;
        private String reason;
        @JSONField(name = "type_desc")
        private String typeDesc;
        @JSONField(name = "apply_remark")
        private String applyRemark;
        private List<String> evidence;
        @JSONField(name = "logistics_time")
        private String logisticsTime;
        @JSONField(name = "logistics_code")
        private String logisticsCode;
        @JSONField(name = "logistics_name")
        private String logisticsName;
        @JSONField(name = "aftersale_id")
        private long aftersaleId;
        @JSONField(name = "aftersale_status")
        private int aftersaleStatus;
        @JSONField(name = "refund_type")
        private int refundType;
        @JSONField(name = "refund_status")
        private int refundStatus;
        @JSONField(name = "post_receiver")
        private String postReceiver;
        @JSONField(name = "post_tel")
        private String postTel;
        @JSONField(name = "post_addr")
        private String postAddr;
        @JSONField(name = "exchange_logistics_code")
        private String exchangeLogisticsCode;
        @JSONField(name = "exchange_logistics_name")
        private String exchangeLogisticsName;
        @JSONField(name = "expire_time")
        private String expireTime;
    }

    @Data
    public static class AftersaleInfo {
        @JSONField(name = "aftersale_type")
        private int aftersaleType;
        @JSONField(name = "aftersale_type_text")
        private String aftersaleTypeText;
    }

    @Data
    public static class ArbitrateInfo {
        @JSONField( name = "arbitrate_id")
        private String arbitrateId;
        @JSONField( name = "arbitrate_state")
        private int arbitrateState;
        @JSONField( name = "evidence_deadline")
        private String evidenceDeadline;
        @JSONField( name = "is_required_evidence")
        private boolean isRequiredEvidence;
        @JSONField( name = "arbitrate_evidence_tmpl")
        private ArbitrateEvidenceTmpl arbitrateEvidenceTmpl;
        private Evidence evidence;
        @JSONField( name = "arbitrate_conclusion")
        private int arbitrateConclusion;
        @JSONField( name = "arbitrate_opinion")
        private String arbitrateOpinion;
        @JSONField( name = "create_time")
        private String createTime;
        @JSONField( name = "update_time")
        private String updateTime;

        @Data
        public static class ArbitrateEvidenceTmpl {
            @JSONField(name = "tmpl_img")
            private String tmplImg;
            @JSONField(name = "evidence_desc")
            private String evidenceDesc;

        }

        @Data
        public static class Evidence {
            @JSONField(name = "evidence_img")
            private String evidenceImg;
            @JSONField(name = "evidence_comment")
            private String evidenceComment;
        }
    }
}
