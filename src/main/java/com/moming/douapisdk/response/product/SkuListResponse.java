package com.moming.douapisdk.response.product;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author lujingpo
 * @date 2022/3/22
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SkuListResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = 8074600258845053714L;

    private List<SkuList> data;

    /**
     * 海关申报要素
     */
    @Data
    public static class CustomsReportInfo {

        /**
         * 条形码
         */
        @JSONField(name = "bar_code")
        private String barCode;

        /**
         * 法定第一计量数量
         */
        @JSONField(name = "first_measure_qty")
        private BigDecimal firstMeasureQty;

        /**
         * 法定第一计量单位
         */
        @JSONField(name = "first_measure_unit")
        private String firstMeasureUnit;

        /**
         * 规格型号
         */
        @JSONField(name = "g_model")
        private String gModel;

        /**
         * 海关代码
         */
        @JSONField(name = "hs_code")
        private String hsCode;

        /**
         * 品牌
         */
        @JSONField(name = "report_brand_name")
        private String reportBrandName;

        /**
         * 品名
         */
        @JSONField(name = "report_name")
        private String reportName;

        /**
         * 法定第二计量数量
         */
        @JSONField(name = "second_measure_qty")
        private BigDecimal secondMeasureQty;

        /**
         * 法定第二计量单位
         */
        @JSONField(name = "second_measure_unit")
        private String secondMeasureUnit;

        /**
         * 售卖单位
         */
        private String unit;

        /**
         * 用途
         */
        private String usage;
    }

    @Data
    public static class SkuList {
        @JSONField(name = "channel_stock_num")
        private int channelStockNum;

        /**
         * sku外部编码
         */
        private String code;

        /**
         * 创建时间
         */
        @JSONField(name = "create_time")
        private Long createTime;

        /**
         * 海关申报要素
         */
        @JSONField(name = "customs_report_info")
        private CustomsReportInfo customsReportInfo;

        /**
         * sku id
         */
        private Long id;

        /**
         * 海南免税：是否套装，0 否，1 是
         */
        @JSONField(name = "is_suit")
        private Integer isSuit;

        /**
         * 库存模型V2新增 普通库存 非活动可售
         */
        @JSONField(name = "normal_stock_num")
        private Long normalStockNum;

        /**
         * app_key
         */
        @JSONField(name = "open_user_id")
        private Long openUserId;

        /**
         * 外部的skuId
         */
        @JSONField(name = "out_sku_id")
        private Long outSkuId;

        /**
         * 预占阶梯库存
         */
        @JSONField(name = "prehold_step_stock_num")
        private Long preholdStepStockNum;

        /**
         * 如果sku_type=0，为空 如果sku_type=1，则为区域仓库存映射表，key为out_warehouse_id，value为占用库存
         */
        @JSONField(name = "prehold_stock_map")
        private Map<String, Object> preholdStockMap;

        /**
         * sku_type=0时，表示预占库存数量 sku_type=1时，使用prehold_stock_map
         */
        @JSONField(name = "prehold_stock_num")
        private Long preholdStockNum;

        /**
         * 价格
         */
        private Long price;

        /**
         * 商品ID
         */
        @JSONField(name = "product_id")
        private Long productId;

        /**
         * 商品 ID 字符串
         */
        @JSONField(name = "product_id_str")
        private String productIdStr;

        /**
         * 活动阶梯库存
         */
        @JSONField(name = "prom_step_stock_num")
        private Long promStepStockNum;

        /**
         * 活动库存
         */
        @JSONField(name = "prom_stock_num")
        private Long promStockNum;

        /**
         * 结算价格
         */
        @JSONField(name = "settlement_price")
        private Long settlementPrice;

        /**
         * 如果sku_type=0，为空 如果sku_type=1，则为区域仓库存映射表，key为out_warehouse_id，value为sku 在对应仓中的发货时效
         */
        @JSONField(name = "ship_rule_map")
        private Map<String, Object> shipRuleMap;

        /**
         * 0-普通0，1-区域库存，10-阶梯库存
         */
        @JSONField(name = "sku_type")
        private Long skuType;

        /**
         * 第一级子规格
         */
        @JSONField(name = "spec_detail_id1")
        private Long specDetailId1;

        /**
         * 第二级子规格
         */
        @JSONField(name = "spec_detail_id2")
        private Long specDetailId2;

        /**
         * 第三级子规格
         */
        @JSONField(name = "spec_detail_id3")
        private Long specDetailId3;

        /**
         * 第一级子规格名
         */
        @JSONField(name = "spec_detail_name1")
        private String specDetailName1;

        /**
         * 第二级子规格名
         */
        @JSONField(name = "spec_detail_name2")
        private String specDetailName2;

        /**
         * 第三级子规格名
         */
        @JSONField(name = "spec_detail_name3")
        private String specDetailName3;

        /**
         * 规格ID
         */
        @JSONField(name = "spec_id")
        private Long specId;

        /**
         * 阶梯库存
         */
        @JSONField(name = "step_stock_num")
        private Long stepStockNum;

        /**
         * 如果sku_type=0，为空 如果sku_type=1，则为区域仓库存映射表，key为out_warehouse_id，value为库存
         */
        @JSONField(name = "stock_map")
        private Map<String, Object> stockMap;
        /**
         * sku_type=0时，表示库存数量 sku_type=1时，使用stock_map
         */
        @JSONField(name = "stock_num")
        private Long stockNum;

        /**
         * 海南免税：套装数量
         */
        @JSONField(name = "suit_num")
        private Long suitNum;

        /**
         * 供应商ID
         */
        @JSONField(name = "supplier_id")
        private String supplierId;

        /**
         * 海南免税：净含量
         */
        private Long volume;
    }
}
