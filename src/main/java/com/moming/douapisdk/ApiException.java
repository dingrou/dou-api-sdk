package com.moming.douapisdk;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.security.PrivilegedActionException;

/**
 * DouYin客户端异常
 *
 * @author tianzong
 * @date 2020/7/23
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class ApiException extends Exception {

    private static final long serialVersionUID = 8585811867368040814L;


    @JSONField(name = "err_no")
    private int errNo;

    private String message;

    public ApiException(int errNo, String message) {
        this.errNo = errNo;
        this.message = message;
    }

    /**
     * Constructs a new throwable with the specified cause and a detail
     * message of {@code (cause==null ? null : cause.toString())} (which
     * typically contains the class and detail message of {@code cause}).
     * This constructor is useful for throwables that are little more than
     * wrappers for other throwables (for example, {@link
     * PrivilegedActionException}).
     *
     * <p>The {@link #fillInStackTrace()} method is called to initialize
     * the stack trace data in the newly created throwable.
     *
     * @param cause the cause (which is saved for later retrieval by the
     *              {@link #getCause()} method).  (A {@code null} value is
     *              permitted, and indicates that the cause is nonexistent or
     *              unknown.)
     * @since 1.4
     */
    public ApiException(Throwable cause) {
        super(cause);
    }
}
